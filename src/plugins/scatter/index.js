var subscribe = []

class Scatter {
  static install( vue, config ) {
    vue.prototype.subscribe = Scatter.subscribe

    document.addEventListener('scatterLoaded', scatterExtension => {
        vue.prototype.scatter = window.scatter
        //console.log(subscribe)
        subscribe.forEach(callback => callback())
    })
  }

  static subscribe(callback) {
    subscribe.push(callback)
  }
}

export default Scatter
