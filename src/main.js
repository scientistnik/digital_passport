import Vue from 'vue'
import App from './App.vue'
import Buefy from 'buefy'
import 'buefy/lib/buefy.css'
import { Scatter } from './plugins'
import "vue-material-design-icons/styles.css"

Vue.use(Buefy)


Vue.config.productionTip = false
Vue.use(Scatter)

new Vue({
  render: h => h(App)
}).$mount('#app')
